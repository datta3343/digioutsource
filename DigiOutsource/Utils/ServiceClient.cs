﻿using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System;

namespace DigiOutsource.Utils
{
    public class ServiceClient
    {
        private readonly string _apiBaseAddress = ConfigurationManager.AppSettings["ApiBaseUrl"];
        public async Task<HttpResponseMessage> GetApiResponseAsync(string apiUrl)
        {
            var client = CreateHttpClient();
            var response = await client.GetAsync(_apiBaseAddress + apiUrl);
            return response;
        }

        public async Task<HttpResponseMessage> PostApiResponseAsync(string apiUrl, string postBody)
        {
            var client = CreateHttpClient();
            var response = await client.PostAsync(_apiBaseAddress + apiUrl,
                new StringContent(postBody, Encoding.UTF8, "application/json"));

            return response;
        }

        public async Task<HttpResponseMessage> PutApiResponseAsync(string apiUrl, string postBody)
        {
            var client = CreateHttpClient();
            var response = await client.PutAsync(_apiBaseAddress + apiUrl,
                new StringContent(postBody, Encoding.UTF8, "application/json"));

            return response;
        }
        public async Task<HttpResponseMessage> DeletetApiResponseAsync(string apiUrl)
        {
            var client = CreateHttpClient();
            var response = await client.DeleteAsync(_apiBaseAddress + apiUrl);
            return response;
        }

        private HttpClient CreateHttpClient(string token = "")
        {
            var httpClient = new HttpClient
            {
            };


            httpClient.DefaultRequestHeaders.Add("TimeZoneId", TimeZoneInfo.Local.Id);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrEmpty(token))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            return httpClient;
        }
    }
}