﻿using DigiOutsource.Models;
using System.Web;

namespace DigiOutsource.Utils
{
    public static class SessionHelper
    {

        private const string CurrentUserKey = "CurrentUser";
        public static LogInUser GetCurrentUser()
        {
            if((LogInUser)HttpContext.Current.Session[CurrentUserKey]==null)
            {
               
            }    
            return (LogInUser)HttpContext.Current.Session[CurrentUserKey];
        }

        public static void SetCurrentUser(LogInUser user)
        {
            HttpContext.Current.Session[CurrentUserKey] = user;
        }

    }
}