﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DigiOutsource.Utils
{
    public class SpyUserValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!HttpContext.Current.User.Identity.Name.ToLower().Contains("digioutsource"))
            {
                //Prevent access to the controller's method and show error page (bad request/forbidden)
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}