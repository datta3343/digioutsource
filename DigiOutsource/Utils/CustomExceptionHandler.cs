﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiOutsource.Utils
{
    public class CustomExceptionHandler : ActionFilterAttribute, IExceptionFilter

    {

        public void OnException(ExceptionContext filterContext)

        {

            Exception e = filterContext.Exception;

            filterContext.ExceptionHandled = true;

            filterContext.Result = new ViewResult()

            {

                ViewName = "Exception"

            };

        }
    }
}