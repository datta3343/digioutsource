﻿using DigiOutsource.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiOutsource.Controllers
{
    [SpyUserValidation]
    [Authorize]
    public class SpyStuffController : Controller
    {
        // GET: SpyStuff
        public ActionResult Index()
        {
            return View();
        }
    }
}