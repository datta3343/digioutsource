﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DigiOutsource.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public void Change(string Langaugeabb)
        {
           
            if(Langaugeabb !=null & Langaugeabb != "tlh")
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Langaugeabb);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Langaugeabb);
            }
            if(Langaugeabb=="tlh")
            {
                CultureInfo ci = CultureInfo.CreateSpecificCulture("tlh-US");
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            HttpCookie cookie = new HttpCookie("Langauge");
            cookie.Value = Langaugeabb;
            Response.Cookies.Add(cookie);

           
        }
    }
}