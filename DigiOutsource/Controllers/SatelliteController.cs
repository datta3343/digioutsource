﻿using DigiOutsource.Models;
using DigiOutsource.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net.Http;
using System.Device.Location;

namespace DigiOutsource.Controllers
{
    [Authorize]
    public class SatelliteController : Controller
    {

        [HttpGet]
        [Authorize]
        public ActionResult Index(int type)
        {
            return View();
        }

        [HttpGet]
        [Authorize]

        public async Task<JsonResult> GetAllSetallites(int type, double latitude = 0.00, double longitude = 0.00)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (type == 1)
                response = await new ServiceClient().GetApiResponseAsync("SatelliteLocations/");
            else
            {
             response = await new ServiceClient().GetApiResponseAsync("SatelliteLocations/VisibleFrom/" + latitude+"/"+ longitude+"/");
            }

            var setallites = await response.Content.ReadAsAsync<List<Setallite>>();
            return Json(new { data = setallites }, JsonRequestBehavior.AllowGet);
        }


    }
}