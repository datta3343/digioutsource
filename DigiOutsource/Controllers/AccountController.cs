﻿using DigiOutsource.Models;
using DigiOutsource.Resources;
using DigiOutsource.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DigiOutsource.Controllers
{
    public class AccountController : Controller
    {
        private string ClientId = ConfigurationManager.AppSettings["Google.ClientID"];
        private string SecretKey = ConfigurationManager.AppSettings["Google.SecretKey"];
        private string RedirectUrl = ConfigurationManager.AppSettings["Google.RedirectUrl"];

        public ActionResult LogIn()
        {
            LogInUser user = new LogInUser();
            return View(user);
        }

        [HttpPost]
        public ActionResult LogIn(LogInUser user)
        {
            if (ModelState.IsValid)
            {
                if (user.Password == "P@ssw0rd123")
                {
                    user.IsExternalLogin = false;
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                      user.Email,
                       DateTime.Now,
                       DateTime.Now.AddMinutes(60),
                       false,
                       FormsAuthentication.FormsCookiePath);

                    string hash = FormsAuthentication.Encrypt(ticket);
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

                 
                    Response.Cookies.Add(cookie);
                    SessionHelper.SetCurrentUser(user);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("Password", Language.PasswordOrUsername);
                    return View(user);
                }

            }
            else
            {
                return View(user);
            }

        }
        public ActionResult LoginUsingGoogle()
        {
          return Redirect($"https://accounts.google.com/o/oauth2/v2/auth?client_id={ClientId}&response_type=code&scope=openid%20email%20profile&redirect_uri={RedirectUrl}&state=abcdef");
        }
        [HttpGet]
        public async Task<ActionResult> SaveGoogleUser(string state="", string code="",  string scope="", string prompt="")
        {
            if (string.IsNullOrEmpty(code))
            {
                return View("Error");
            }

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            var requestUrl = $"oauth2/v4/token?code={code}&client_id={ClientId}&client_secret={SecretKey}&redirect_uri={RedirectUrl}&grant_type=authorization_code";

            var dict = new Dictionary<string, string>
            {
                { "Content-Type", "application/x-www-form-urlencoded" }
            };
            var req = new HttpRequestMessage(HttpMethod.Post, requestUrl) { Content = new FormUrlEncodedContent(dict) };
            var response = await httpClient.SendAsync(req);
            var token = JsonConvert.DeserializeObject<GmailToken>(await response.Content.ReadAsStringAsync());
            var user = await GetuserProfile(token.AccessToken);
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                      user.Email,
                       DateTime.Now,
                       DateTime.Now.AddMinutes(60),
                       false,
                       FormsAuthentication.FormsCookiePath);

            string hash = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

            Response.Cookies.Add(cookie);
            user.IsExternalLogin = true;
            SessionHelper.SetCurrentUser(user);
            return RedirectToAction("Index", "Home");
         }
        public async Task<LogInUser> GetuserProfile(string accesstoken)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            string url = $"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={accesstoken}";
            var response = await httpClient.GetAsync(url);
            return JsonConvert.DeserializeObject<LogInUser>(await response.Content.ReadAsStringAsync());
        }
        
        [Authorize]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            //if (SessionHelper.GetCurrentUser().IsExternalLogin)
            //{
            //    Session.Remove("CurrentUser");
            //    Response.Redirect("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout" + "?continue=http://localhost:59662/");
            //}

            Session.Remove("CurrentUser");

            return RedirectToAction("Login");
        }
    }
}