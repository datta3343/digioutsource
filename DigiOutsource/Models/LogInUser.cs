﻿using DigiOutsource.Resources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiOutsource.Models
{
    public class LogInUser
    {
        [Required(ErrorMessageResourceType = typeof(Language),ErrorMessageResourceName = "PasswordRequiredValidation")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(Language),ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceType = typeof(Language), ErrorMessageResourceName = "EmailValidation")]
        public string Email { get; set; }

        public bool IsExternalLogin { get; set; }

    }
    public class GmailToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }

        [JsonProperty("id_token")]
        public string IdToken { get; set; }
    }
}