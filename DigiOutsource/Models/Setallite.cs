﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiOutsource.Models
{
    public class Setallite
    {
        public List<Path> Path = new List<Path>();
        public Orbit orbit = new Orbit();
        public int id { get; set; }
        public int prn { get; set; }
        public string constellation { get; set; }
        public string displayName { get; set; }
    }

    public class Path
    {
        public DateTime asAt { get; set; }
        public Trace trace = new Trace();

    }

    public class Trace
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string height { get; set; }
    }
    public class Orbit
    {
        public int satId { get; set; }
        public int healthCode { get; set; }
        public string eccentricity { get; set; }
        public string deltaInclination { get; set; }
        public string rootOfSemiMajorAxis { get; set; }
        public string a0 { get; set; }
        public string a1 { get; set; }
        public string omega0 { get; set; }
        public string omega { get; set; }
        public string m0 { get; set; }
        public string omegaDot { get; set; }
        public string gpsSeconds { get; set; }
        public string gpsWeek { get; set; }
        public bool isHealthy { get; set; }
    }
}



