﻿using DigiOutsource.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DigiOutsource.Services
{
    public class RequestService : IRequestService
    {
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly string _baseUrl;

        public RequestService(string baseUrl)
        {
            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore
            };

            _baseUrl = baseUrl;
             _serializerSettings.Converters.Add(new StringEnumConverter());
        }

        public async Task<TResult> GetAsync<TResult>(string uri)
        {
            try
            {
                HttpClient httpClient = CreateHttpClient();
                HttpResponseMessage response = await httpClient.GetAsync(_baseUrl + uri);

                await HandleResponse(response);

                string serialized = await response.Content.ReadAsStringAsync();
                TResult result = await Task.Run(() => JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
            catch (Exception ex)
            {
                var message = $"URI: {uri}\nException: {ex}";
                var exp = new Exception(message);
                throw exp;
            }
        }

        public async Task<TResult> DeleteAsync<TResult>(string uri)
        {
            try
            {
                HttpClient httpClient = CreateHttpClient();
                HttpResponseMessage response = await httpClient.DeleteAsync(_baseUrl + uri);

                await HandleResponse(response);

                string serialized = await response.Content.ReadAsStringAsync();
                TResult result = await Task.Run(() => JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));

                return result;
            }
            catch (Exception ex)
            {
                var message = $"URI: {uri}\nException: {ex}";
                var exp = new Exception(message);
                throw exp;
            }
        }

        public Task<TResult> PostAsync<TResult>(string uri, TResult data)
        {
            return PostAsync<TResult, TResult>(uri, data);
        }

        public async Task<TResult> PostAsync<TRequest, TResult>(string uri, TRequest data)
        {
            string serialized = string.Empty;
            try
            {
                HttpClient httpClient = CreateHttpClient();
                serialized = await Task.Run(() => JsonConvert.SerializeObject(data, _serializerSettings));
                HttpResponseMessage response = await httpClient.PostAsync(_baseUrl + uri, new StringContent(serialized, Encoding.UTF8, "application/json"));

                await HandleResponse(response);

                string responseData = await response.Content.ReadAsStringAsync();

                return await Task.Run(() => JsonConvert.DeserializeObject<TResult>(responseData, _serializerSettings));
            }
         
            catch (Exception ex)
            {
                var message = $"URI: {uri}\nData: {serialized}\nException: {ex}";
                var exp = new Exception(message);

                throw exp;
            }
        }

        public Task<TResult> PutAsync<TResult>(string uri, TResult data)
        {
            return PutAsync<TResult, TResult>(uri, data);
        }

        public async Task<TResult> PutAsync<TRequest, TResult>(string uri, TRequest data)
        {
            string serialized = string.Empty;
            try
            {
                HttpClient httpClient = CreateHttpClient();
                serialized = await Task.Run(() => JsonConvert.SerializeObject(data, _serializerSettings));
                HttpResponseMessage response = await httpClient.PutAsync(_baseUrl + uri, new StringContent(serialized, Encoding.UTF8, "application/json"));

                await HandleResponse(response);

                string responseData = await response.Content.ReadAsStringAsync();

                return await Task.Run(() => JsonConvert.DeserializeObject<TResult>(responseData, _serializerSettings));
            }
        
            catch (Exception ex)
            {
                var message = $"URI: {uri}\nData: {serialized}\nException: {ex}";
                var exp = new Exception(message);
                throw exp;
            }
        }

        private HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient
            {
            };


            httpClient.DefaultRequestHeaders.Add("TimeZoneId", TimeZoneInfo.Local.Id);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return httpClient;
        }


        private async Task HandleResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Forbidden ||
                    response.StatusCode == HttpStatusCode.Unauthorized ||
                    response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception(content);
                }
               
                throw new HttpRequestException(content);
            }
        }
    }
}